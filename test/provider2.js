/**
 * Created by Dirk on 19.02.2016.
 */
'use strict';

// constructor
function Provider (configObject) {
  if (!(this instanceof Provider)) {
    return new Provider(configObject);
  }

  // public interface
  return {
    name: configObject.name,
    myMethod: function (text) {
      return text;
    },
    add: function (x, y) {
      return x + y;
    }
  };
}

module.exports = Provider;

