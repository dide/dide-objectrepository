/**
 * Created by Dirk on 20.02.2016.
 * using chai assert http://chaijs.com/api/assert/
 */
var assert = require('chai').assert;
var objecRepository;

describe('objectbroker', function() {
  before(function(){
    // The before() callback gets run before all tests in the suite. Do one-time setup here.
    objecRepository = require('./../objectrepository');
    objecRepository.resetMultiInstanceCounter();
  });
  beforeEach(function(){
    // The beforeEach() callback gets run before each test in the suite.
  });

  it('check that the ObjectBroker has all required methods', function() {
    assert.isFunction(objecRepository.addInstance, 'ObjectBroker has the addInstance Method');
    assert.isFunction(objecRepository.getObject, 'ObjectBroker has the getObject Method');
    assert.isFunction(objecRepository.resetMultiInstanceCounter, 'ObjectBroker has the resetMultiInstanceCounter Method');
  });

  it('register singleton provider', function () {
    // register instance
    var Provider1 = require('./provider1');
    objecRepository.addInstance('si', Provider1, { name: 'Singleton' }, { isSingleton: true, canOverwrite: false, force: false } );
    assert.isDefined(objecRepository.si, "Object has the singleton definition registered");
  });

  it('register multiinstance provider', function () {
    // register instance
    var Provider1 = require('./provider1');
    objecRepository.addInstance('mi', Provider1, { name: 'Singleton' }, { isSingleton: false, canOverwrite: false, force: false } );
    assert.isDefined(objecRepository.mi, "Object has the multi instance definition registered");
  });

  it('create a single instance object', function () {
    var obj1a = objecRepository.si.create();
    assert.isDefined(obj1a, "instance is defined");
    assert.isDefined(obj1a.instanceInfo, "instance info is defined");
    assert.isDefined(obj1a.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj1a.instanceInfo.repositorySettings.isSingleton, "instance info.isSingleton is defined");
    assert.isDefined(obj1a.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj1a.instanceInfo.repositorySettings.isSingleton, true, "instance info.paramObjects.repositorySettings.isSingleton");
    assert.equal(obj1a.instanceInfo.instanceId, 0, "instance info.paramObjects.instanceId is 0");
  });

  it('create a 2nd single instance object', function () {
    var obj1b = objecRepository.si.create();
    assert.isDefined(obj1b, "instance is defined");
    assert.isDefined(obj1b.instanceInfo, "instance info is defined");
    assert.isDefined(obj1b.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj1b.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj1b.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj1b.instanceInfo.repositorySettings.isSingleton, true, "instance info.paramObjects.repositorySettings.isSingleton");
    assert.equal(obj1b.instanceInfo.instanceId, 0, "instance info.paramObjects.instanceId is 0");
  });
  
  it('create a first multi instance object', function () {
    var obj2 = objecRepository.mi.create();
    assert.isDefined(obj2, "instance is defined");
    assert.isDefined(obj2.instanceInfo, "instance info is defined");
    assert.isDefined(obj2.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj2.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj2.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj2.instanceInfo.repositorySettings.isSingleton, false, "instance info.paramObjects.repositorySettings.isSingleton is false");
    assert.equal(obj2.instanceInfo.instanceId, 1, "instance info.paramObjects.instanceId is 1");
  });

  it('create a second multi instance object', function () {
    var obj3 = objecRepository.mi.create();
    assert.isDefined(obj3, "instance is defined");
    assert.isDefined(obj3.instanceInfo, "instance info is defined");
    assert.isDefined(obj3.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj3.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj3.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj3.instanceInfo.repositorySettings.isSingleton, false, "instance info.paramObjects.repositorySettings.isSingleton is false");
    assert.equal(obj3.instanceInfo.instanceId, 2, "instance info.paramObjects.instanceId is 2");
  });

  it('create a second singleinstance object', function () {
    var objectRepository2 = require('./../objectrepository');
    var obj1c = objectRepository2.si.create();
    assert.isDefined(obj1c, "instance is defined");
    assert.isDefined(obj1c.instanceInfo, "instance info is defined");
    assert.isDefined(obj1c.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj1c.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj1c.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj1c.instanceInfo.repositorySettings.isSingleton, true, "instance info.paramObjects.repositorySettings.isSingleton");
    assert.equal(obj1c.instanceInfo.instanceId, 0, "instance info.paramObjects.instanceId is 0");
  });

  it('create a second multi instance object', function () {
    var objectRepository2 = require('./../objectrepository');
    var obj4 = objectRepository2.mi.create();
    assert.isDefined(obj4, "instance is defined");
    assert.isDefined(obj4.instanceInfo, "instance info is defined");
    assert.isDefined(obj4.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj4.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj4.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj4.instanceInfo.repositorySettings.isSingleton, false, "instance info.paramObjects.repositorySettings.isSingleton");
    assert.equal(obj4.instanceInfo.instanceId, 3, "instance info.paramObjects.instanceId is 3");

  });

  it('check the getObject method', function () {
    var objectRepository4 = require('./../objectrepository');
    assert.isDefined(objectRepository4.getObject.bind(objectRepository4, 'si'), 'get single instance without the id as parameter');
    assert.throws(objectRepository4.getObject.bind(objectRepository4, 'si', 'zero is a wrong parameter'), 'instanceId must be a number of must be not set to get the defaul value 0, value was: zero is a wrong parameter', 'string as instanceId is not allowed');
    assert.isDefined(objectRepository4.getObject.bind(objectRepository4, 'si', '0'), 'allow to handover the id as string');

    assert.throws(objectRepository4.getObject.bind(objectRepository4, 'xy', 0),'objectName is not registered: xy', 'single instance object');
    assert.throws(objectRepository4.getObject.bind(objectRepository4, 'mi', 0), 'objectName mi has not the instanceId: 0', 'can get a multiInstance Object by his name and Id');
    assert.isDefined(objectRepository4.getObject('si', 0), 'single instance object');
    assert.throws(objectRepository4.getObject.bind(objectRepository4, 'si', 1), 'the requested object is a singleton and can only have the instanceId 0');
    assert.isDefined(objectRepository4.getObject('mi', 3), 'can get a multiInstance Object by his name and Id');
  });


  it('check the resetMultiInstanceCounter method', function () {
    assert.isTrue(objecRepository.resetMultiInstanceCounter('mi'), "counter mi reset successfully");
    assert.isFalse(objecRepository.resetMultiInstanceCounter('si'), "counter si reset unsuccessfully, as it's a singleton");
    assert.isFalse(objecRepository.resetMultiInstanceCounter('xy'), "counter xy reset unsuccessfully, as this object is unknown");

    var objectRepository3 = require('./../objectrepository');
    var obj5 = objectRepository3.mi.create();
    assert.isDefined(obj5, "instance is defined");
    assert.isDefined(obj5.instanceInfo, "instance info is defined");
    assert.isDefined(obj5.instanceInfo.name, "instance info.name is defined");
    assert.isDefined(obj5.instanceInfo.repositorySettings.isSingleton, "instance info.repositorySettings.isSingleton is defined");
    assert.isDefined(obj5.instanceInfo.paramObjects, "instance info.paramObjects is defined");
    assert.equal(obj5.instanceInfo.repositorySettings.isSingleton, false, "instance info.paramObjects.repositorySettings.isSingleton");
    assert.equal(obj5.instanceInfo.instanceId, 1, "instance info.paramObjects.instanceId is 1");
  });
  
  it ('check the overwrite possibilities', function () {
    var Provider1old = require('./provider1');
    var objectName = 'siOverwriteFalse';
    objecRepository.addInstance(objectName, Provider1old, { name: 'Singleton overwrite False' }, { isSingleton: true, canOverwrite: false, force: false } );
    assert.isDefined(objecRepository[objectName], "Object has the singleton definition registered");

    //try to overwrite the current one
    var Provider12nd = require('./provider1');
    assert.throws(
      objecRepository.addInstance.bind(objecRepository, objectName, Provider12nd,
        { name: 'Singleton overwrite False with new object' },
        { isSingleton: true, canOverwrite: false, force: true }
      ),
      'the objectName sioverwritefalse was already used and the first registration do not allow to overwrite it',
      'can not overwrite already declared definitions as this is defined as blocked'
    );

    objectName = 'siOverwriteTrue';
    objecRepository.addInstance(objectName, Provider1old, { name: 'Singleton overwrite true' }, { isSingleton: true, canOverwrite: true, force: false } );
    assert.isDefined(objecRepository[objectName], "Object has the singleton definition registered");

    //try to overwrite the current one without force
    assert.throws(
      objecRepository.addInstance.bind(objecRepository, objectName, Provider12nd,
        { name: 'Singleton overwrite False with new object' },
        { isSingleton: true, canOverwrite: false, force: false }
      ),
      'the objectName sioverwritetrue was already used and if you like to overwrite it you must set the force setting to overwrite it',
      'can not overwrite already declared definitions as the method was invoked without force parameter set to true'
    );

    assert.doesNotThrow(
      objecRepository.addInstance.bind(objecRepository, objectName, Provider12nd,
        { name: 'Singleton overwrite False with new object' },
        { isSingleton: true, canOverwrite: false, force: true }
      ),
      'the objectName sioverwritetrue was already used and if you like to overwrite it you must set the force setting to overwrite it',
      'can overwrite already declared definitions as the method was invoked with force parameter set to true'
    );


  });


  after(function() {
    // after() is run after all your tests have completed. Do teardown here.
  });

});
