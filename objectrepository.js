/**
 * Created by Dirk on 19.02.2016.
 * base on the singleton patter https://simplapi.wordpress.com/2012/05/14/node-js-singleton-structure/
 */
'use strict';

var singleton = function singleton(){
  //defining a var instead of this (works for variable & function) will create a private definition
  var definitions = {};

  var createNewInstanceDefinition = function (objectName, objectDefinition, instanceParameters, repositorySettings) {
    definitions[objectName] = {
      instanceInfo: {
        name: objectName,
        repositorySettings: repositorySettings,
        paramObjects: instanceParameters
      },
      objectDefinition: objectDefinition
    };
    //add a method with the objectName
    var instance;
    var definition = definitions[objectName];
    if (repositorySettings.isSingleton) {
      // create a new instance
      instance = new definition.objectDefinition(definition.instanceInfo.paramObjects);
      // add the instanceInfo
      instance.instanceInfo = definition.instanceInfo;
      // add the id 0 as it's a singleton and we always give back the same id
      instance.instanceInfo.instanceId = 0;
      // we save the instance for future requests
      definition.instance = instance;
      // we create the method create for future use
      definitions[objectName].create = function () {
        return definitions[objectName].instance;
      };
    } else {
      // we have to create a method to create always a new instance
      definitions[objectName].create = (function () {
        // create a new instance
        instance = new definition.objectDefinition(definition.instanceInfo.paramObjects);
        // add the instanceInfo
        instance.instanceInfo = definition.instanceInfo;
        // we use a simple instanceCounter as id for each object
        if (definition.instanceCounter === undefined || definition.instanceCounter === null || isNaN(definition.instanceCounter) ) {
          // init with 0
          definition.instanceCounter = 0;
        }
        // increment the counter
        definition.instanceCounter++;
        // add the id
        instance.instanceInfo.instanceId = definition.instanceCounter;
        if (!definition.hasOwnProperty('instances')){
          definition.instances = {};
        }
        definition.instances[instance.instanceInfo.instanceId] = instance;
        return instance;
      });
    }
  };

  /* public methods*/

  /***
   * adding a new instance
   * @param objectName the name where you can access later the instance. it will automatically add as property to the objectRepository object. I recommend to use this new created property over the method call getObject
   * @param objectDefinition the definition self, most of cases its the result of an require
   * @param instanceParameters the parameter definition to create the instance
   * @param repositorySettings, a object with the settings
   *  isSingleton: true: always return the same instance, false: always return a fresh instance. default: true
   *  canOverwrite: false: the same objectName can't be used again. on try it will create an error, true: the same object name can be reused. you should understand that this may have an impact of the hole application. default: false
   *  force: false: we throw an error if you try to overwrite an existing member to avoid accidental overwrite a previous definition, true: you can overwrite an objectName if the canOverwrite parameter is set to true. default: false
   */
  this.addInstance = function (objectName, objectDefinition, instanceParameters, repositorySettings) {
    if (repositorySettings === null || repositorySettings === undefined) {
      // set all the defaults
      repositorySettings = {
        isSingleton: true,
        canOverwrite: false,
        force: false
      };
    } else {
      if (!repositorySettings.hasOwnProperty('isSingleton')) {
        repositorySettings.isSingleton = true;
      }
      if (!repositorySettings.hasOwnProperty('canOverwrite')) {
        repositorySettings.canOverwrite = false;
      }
      if (!repositorySettings.hasOwnProperty('force')) {
        repositorySettings.force = false;
      }
    }

    //ensure it's case insensitive
    var objectNameKey = objectName.toLowerCase();
    if (!definitions.hasOwnProperty(objectNameKey)) {
      // not yet defined, create a new definition
      this[objectName] = {};
      createNewInstanceDefinition(objectNameKey, objectDefinition, instanceParameters, repositorySettings);
      this[objectName].create =definitions[objectNameKey].create;
    } else {
      //already exist?
      var definition = definitions[objectNameKey];
      // check
      if (!definition.instanceInfo.repositorySettings.canOverwrite) {
        throw new Error('the objectName ' + objectNameKey + ' was already used and the first registration do not allow to overwrite it');
      }
      if (definition.instanceInfo.repositorySettings.canOverwrite && !repositorySettings.force) {
        throw new Error('the objectName ' + objectNameKey + ' was already used and if you like to overwrite it you must set the force setting to overwrite it');
      }
      console.warn('We overwrite the objectName ' + objectNameKey + ' with a new definition. The old object was set to allow to owerwrite and the force parameter was used');
      // overwrite the existing one
      this[objectName] = {};
      createNewInstanceDefinition(objectNameKey, objectDefinition, instanceParameters, repositorySettings);
      this[objectName].create =definitions[objectNameKey].create;
    }
  };

  /***
   * reset the multiIntanceCounter, be sure that you not create use the id as the id can be double after reset
   * @objectName, an optional parameter to reset only one objectCounter, if you omit this parameter all multi instance instances will be reseted
   */
  this.resetMultiInstanceCounter = function (objectName) {
    var key;
    if (objectName === null || objectName === undefined) {
      for (key in definitions) {
        var definition = definitions[key];
        // ignore if it's a singleton as there is no counter required
        if (definition.instanceInfo.repositorySettings.isSingleton) {
          continue;
        }
        //only reset the counters from are multi instance objects
        definition.instanceCounter = 0;
      }
      return true;
    } else {
      //ensure it's case insensitive
      objectName = objectName.toLowerCase();
      if (definitions.hasOwnProperty(objectName)) {
        if (definitions[objectName].instanceInfo.repositorySettings.isSingleton) {
          return false;
        }
        definitions[objectName].instanceCounter = 0;
        return true;
      } else {
        return false;
      }
    }
  };

  /***
   *
   * @param objectName
   * @param instanceId
   */
  this.getObject = function (objectName, instanceId) {
    if (instanceId === null || instanceId === undefined) {
      //set the default value
      instanceId = 0;
    }
    if (isNaN(instanceId)) {
      throw new Error('instanceId must be a number of must be not set to get the defaul value 0, value was: ' + instanceId);
    }

    //ensure it's a number
    instanceId = parseInt(instanceId);

    //ensure it's case insensitive
    objectName = objectName.toLowerCase();
    if (!definitions.hasOwnProperty(objectName)) {
      throw new Error('objectName is not registered: ' + objectName);
    }
    var definition = definitions[objectName];
    if (definition.instanceInfo.repositorySettings.isSingleton) {
      if (instanceId !== 0) {
        throw new Error('the requested object is a singleton and can only have the instanceId 0');
      }
      return definition.instance;
    }
    if (!definition.hasOwnProperty('instances')) {
      //instances has never registered
      throw new Error('objectName has no active instance: ' + objectName);
    }
    if (!definition.instances.hasOwnProperty(instanceId)) {
      throw new Error('objectName ' + objectName + ' has not the instanceId: ' + instanceId);
    }
    return definition.instances[instanceId];
  };

  //todo: not working?
  /*
  if(singleton.caller != singleton.getInstance){
    throw new Error("This object cannot be instanciated");
  }
  */
};

/* ************************************************************************
 SINGLETON CLASS DEFINITION
 ************************************************************************ */
singleton.instance = null;

/**
 * Singleton getInstance definition
 * @return singleton class
 */
singleton.getInstance = function(){
  if(this.instance === null){
    this.instance = new singleton();
  }
  return this.instance;
};

module.exports = singleton.getInstance();
