# dide-objectrepository

The objectrepository was developed as try out to find out if there is a solution for the following problem:
- easy possibility to use any kind of previous defined object at any time of the project
- I like to have the possibility to have multiple instances from the same object
- I like to be able to "extend" an existing object by overwriting it so that a new functions may will be available.


The first can be done out of the box with node by using require but the 2nd one is a mor tricky one. So I wrote a small broker that allows that you solve both wishes.

During the first phase of initialization of your application you will first register all objects you wish to use later. On time of the registration you define the settings where will be used to create the instance and you also set if the object is a singleton or a multiple instance type.

In your own modules you can require now the objectrepository as this is a singleton. We add to the objectrepository a property with the name of your object and this property has the method create to create an instance. Depending of your setting (singleton or multiple) you get an instance back that you can use in your application

to see a full example ti would be best you look on the test cases.


## sample code of a module definition (provider1.js)

```js

'use strict';

// constructor
function Provider (configObject) {
  if (!(this instanceof Provider)) {
    return new Provider(configObject);
  }

  // public interface
  return {
    name: configObject.name,
    myMethod: function (text) {
      return text;
    },
    add: function (x, y) {
      return x + y;
    }
  };
}

module.exports = Provider;

```

## sample code of a module definition (provider2.js)

```js

'use strict';

// constructor
function Provider (configObject) {
  if (!(this instanceof Provider)) {
    return new Provider(configObject);
  }

  // public interface
  return {
    name: configObject.name,
    myMethod: function (text) {
      return text;
    },
    add: function (x, y) {
      return x + y;
    }
  };
}

module.exports = Provider;

```


## sample code of a module that use the objectrepository

```js

var objectrepository = require('./objectbroker');

//add new dependency
var Provider1 = require('./provider1');
var Provider2 = require('./provider1');

// register instance
objectrepository.addInstance(
    'si', 
    Provider1, 
    { 
        name: 'Singleton' 
    }, 
    { 
        isSingleton: true, 
        canOverwrite: false, 
        force: false 
    }
);
objectrepository.addInstance(
    'mi', 
    Provider2, 
    { 
        name: 'Multiinstance' 
    },
    { 
        isSingleton: false, 
        canOverwrite: false, 
        force: false 
    } 
);

console.log();

// singleton
var obj1a = objectrepository.si.create();
console.log('instanceInfo: ' + JSON.stringify(obj1a.instanceInfo));
console.log('myMethod: ' + obj1a.myMethod("Singleton object 1"));
console.log('add result: ' + obj1a.add(1,3));
console.log();

var obj1b = objectrepository.si.create();
console.log('instanceInfo: ' + JSON.stringify(obj1b.instanceInfo));
console.log('myMethod: ' + obj1b.myMethod("Singleton object 2"));
console.log('add result: ' + obj1b.add(1,3));
console.log();


//new instance
var obj2 = objectrepository.mi.create();
console.log('instanceInfo: ' + JSON.stringify(obj2.instanceInfo));
console.log('myMethod: ' + obj2.myMethod("instance 1"));
console.log('add result: ' + obj2.add(2,3));
console.log();

//new instance
var obj3 = objectrepository.mi.create();
console.log('instanceInfo: ' + JSON.stringify(obj3.instanceInfo));
console.log('myMethod: ' + obj3.myMethod("instance 2"));
console.log('add result: ' + obj3.add(3,3));
console.log();

//new instance
var obj4 = objectrepository.mi.create();
console.log('instanceInfo: ' + JSON.stringify(obj4.instanceInfo));
console.log('myMethod: ' + obj4.myMethod("instance 3"));
console.log('add result: ' + obj4.add(4,3));
console.log();

```

There are may other possibilites and also more options available like protecting for a kind of overwriting or the possibility for overwriting. 
Let's assume you have a logger definition that you like to use all over your application to write into a log file, nothing very special. 
You just initalize it as described but later in your project you realize that it would be nice if you want to have a logger that have more functions like I had, write all log messages over rabbitMQ to a centralize system.
Yes, you can easy overwrite the first declaration and create a new object definition with the new one.
BUT I like to have the both:
 I like that my new class loggerWithRabbitMq can benefit from the default logger by using inside the logger but then I like to overwrite the regular logger with my new one. So the application do not change the name of the object, but get a different behavior.
 
 
Please be very carefully to understand the impact of such "overwrite" of objects. The good news, your are able to stop overwriting your own object inside of the objectrepository on a later time if you don't want it. To allow the overwriting you have to explicit give free this possibility as it's turned of by default it's turned off.
Also you should be aware that you ma have to only extend the current object and not try to complete exchange all methods and be aware that you may breach the original interface. 
So, there is a possibility, but you don't necessary have to go this way.

enjoy it, love to get feedback
